sap.ui.define([
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/core/BusyIndicator"
], function(MessageBox, MessageToast, BusyIndicator) {
	"use strict";
	
	return {
	    
	    _showBI: function(value){
			if(value){
				BusyIndicator.show(0);
			} else {
				BusyIndicator.hide();
			}
		},
		
		_onShowMessage: function(_message, _type) {
			try{
				if(_message !== undefined && _type !== undefined){
					if (_type === "info") {
						MessageBox.information(_message,{
							styleClass: this.styleClass
						});
					} else if (_type === "error") {
						MessageBox.error(_message,{
							styleClass: this.styleClass
						});
					} else if (_type === "warn") {
						MessageBox.warning(_message,{
							styleClass: this.styleClass
						});
					} else if (_type === "toast") {
						MessageToast.show(_message);
					} else if (_type === "done") {
						MessageBox.success(_message,{
							styleClass: this.styleClass
						});
					}
				}else{
					this.console.warn("_message or _type are undefined");
				}
			} catch(err){
				this.console.warn(err.stack);
			}
		},
		
		_showHTML: function(html){
			try {
				var _text = html.response.body;
				var _message = "\n" + _text.replace(/<[^>]*>?/g, " ") + "\n\n";
				
				if (html.response.statusCode > 399 && html.response.statusCode < 600){
					MessageBox.error(_message,{
							styleClass: this.styleClass
						});
				} else {
					MessageBox.information(_message,{
							styleClass: this.styleClass
						});
				}
			} catch (err){
				this.console.warn(err.stack);
			}
			
		}
	    
	};
	
});