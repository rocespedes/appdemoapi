sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"mqa/ui5/AppDemoAPI/util/Util",
	"mqa/ui5/AppDemoAPI/util/Constants"
], function (Controller, JSONModel, Util, Constants) {
	"use strict";

	return Controller.extend("mqa.ui5.AppDemoAPI.controller.Home", {
		hanadb : "/hanadb/logical",
		s4h: new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/"),
		s4hdb: new sap.ui.model.odata.ODataModel("/hanadb/logical"),
		
		onInit: function() {
			var sRootPath = jQuery.sap.getModulePath("mqa.ui5.AppDemoAPI");
			var oModel = new JSONModel([sRootPath, "/model/model.json"].join("/"));
			
			this.getView().setModel(oModel);
			
			this._getTickets(Constants._ODATA_PINKTICKET, "/Tickets");
			
			this.getTurns();
		},
		
		getTurns: function(){
			var that = this;

			jQuery.ajax({
				url: this.hanadb + "/xs_service/pl_turnos.xsjs",
				method: "GET",
				success: function (data) {
					if (data.code === 1) {
						that.getView().getModel().setProperty("/turns", data.data);
						that.getView().getModel().updateBindings();
					}
				},
				error: function (err) {
					
				}
			});
		},
		
		_getTickets: function (sService, sPath, Status) {

			var that = this;

			this.s4hdb.read(sService, {
				async: false,
				success: function (oData) {
					if (oData.results.length > 0) {
						that.getView().getModel().setProperty(sPath, oData.results);
						that.getView().getModel().updateBindings();
					} else {
						//Util._onShowMessage(i18n.getText("WarningMessage"), "error");
					}

				},
				error: function (err) {
					Util._showHTML(err);
					Util._showBI(false);

				}
			});

		}
		
	});
});